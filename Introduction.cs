// Using System Namespace
using System;
using System.Collections.Generic;

namespace Tutorial
{
    class Program
    {
        //Main Entry Point of application
        public static void Main(string[] args)
        {
          

            Cage cage = new Cage(new Dog());
            cage.add(new Cage(new Cat()));
            cage.add(new Cage(new Cat()));
            cage.add(new Cage(new Cat()));
            cage.add(new Cage(new Cat()));
            cage.add(new Cage(new Dog()));
            cage.add(new Cage(new Cat()));
            cage.add(new Cage(new Cat()));
            cage.add(new Cage(new Cat()));
            cage.add(new Cage(new Dog()));

            cage.allSpeak();

        }
    }
}


