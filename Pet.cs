// Using System Namespace
using System;
namespace Tutorial
{
    

    public abstract class Pet
    {
        public abstract void speak();
        internal void eat()
        {

        }
        private void sleep()
        {
               
        }
        
    }

    public class Dog:Pet{
        public override void speak()
        {
            Console.WriteLine("arf");
        }
    
    }
    public class Cat:Pet{
        public override void speak()
        {
            Console.WriteLine("meow");
        }
    }

    public class Cage
    {
        public Pet pet;
        public Cage nextCage;

        public Cage(Pet _pet)
        {
            pet = _pet;
        }

        public void add(Cage cage)
        {
            Cage tempCage = this; 
            while(tempCage.nextCage != null)
            {
               // tempCage=this;
                tempCage = tempCage.nextCage;
            }
            tempCage.nextCage = cage;
        }

        public void allSpeak()
        {
            Cage tempCage = this; 
            while(tempCage.nextCage != null)
            {
               // tempCage=this;
                tempCage.pet.speak();
                tempCage = tempCage.nextCage;
            }
                tempCage.pet.speak();
        }
    }
}
        
        
   

  

    
